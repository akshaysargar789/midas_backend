from flask import Flask, render_template
from flask_mongokit import MongoKit
from flask_mail import Mail, Message
import os
from flask_cors import CORS

app = Flask(__name__)

CORS(app)

app.config.from_object('conf.mainconf.DevelopmentConfig')

app.config.update(dict(
    DEBUG = True,
    MAIL_SERVER = 'smtp.gmail.com',
    MAIL_PORT = 465,
    MAIL_USE_TLS = False,
    MAIL_USE_SSL = True,
    MAIL_USERNAME = 'asargar7@gmail.com',
    MAIL_PASSWORD = 'fcchelsea10',
))

db = MongoKit(app)

mail = Mail(app)

import api
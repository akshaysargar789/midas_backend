from flask import Response, request, abort, redirect, render_template
from app import app, db, mail
from flask_mail import Message
from bson import json_util
import json
from libs import tokenize
from libs.decorators import *
import requests
import datetime
import time

@app.route('/dashboard', methods=["GET"])
@authentication_required
def getDashboardData(**kwargs):
    if kwargs['payload']['role'] == "user":
        device_list = db.Device.getAllDevicesByUserEmail(kwargs['payload']['email'])
        dashboard_stats = []

        for device in device_list['devices']:
            latest_document = db.Dashboard.getLatestDocument(device['serial_id'])
            if latest_document['recent_data']:
                marked_data = latest_document['recent_data'][0]['marked_data']
                layout_name = latest_document['recent_data'][0]['layout_name']
                marked_data_stats = db.Dashboard.getMarkedDataStats(marked_data)
                if marked_data_stats['status']:
                    marked_data_stats['marked_data_stats']['layout_name'] = layout_name
                    device['marked_data_stats'] = marked_data_stats['marked_data_stats']
                    dashboard_stats.append(device)

        return Response(json.dumps({
                "status": True,
                "data": dashboard_stats,
                "message": "Successfully fetched dashboard stats"
            }, default=json_util.default), 
            mimetype="application/json")
    else:
        return Response(json.dumps({
                "status": False,
                "message": "Error fetching dashboard stats"
            }, default=json_util.default), 
            mimetype="application/json")
from flask import Flask, render_template, Response, request
from app import app, db
import json
from bson import ObjectId
from flask_mongokit import MongoKit, Document
import datetime
import time

class Dashboard(Document):
    __collection__ = "dashboard"
    structure = {
        "name": str,
    }

    def addDeviceData(self, device_data):
        try:
            device_data_id = db.data.insert(device_data)
            return {
                    "status": True,
                    "message": "Device data added sucessfully"
                }

        except Exception, e:
            return {
                "status": False,
                "message": "Device data addition failed %s" %e
            }
    
    def getLatestDocument(self, serial_id):
        try:
            recent_data = list(db.data.find({'serial_id': serial_id}).sort([('_id', -1)]).limit(1))
            return {
                    "status": True,
                    "recent_data": recent_data,
                    "message": "Device data added sucessfully"
                }

        except Exception, e:
            return {
                "status": False,
                "message": "Device data addition failed %s" %e
            }

    def getMarkedDataStats(self, marked_data):
        try:
            marked_data_list = list(db.data.find({'marked_data': marked_data}))
            total_operation_time = 0
            marked_data_count = 0
            marked_data_name = None

            for single_data_index, single_data in enumerate(marked_data_list):
                total_operation_time += single_data['operation_time']
                marked_data_count = single_data_index
                marked_data_name = single_data['marked_data']

            marked_data_stats={
                "total_operation_time": total_operation_time,
                "marked_data_count": marked_data_count + 1,
                "marked_name": marked_data_name
            }

            return {
                    "status": True,
                    "marked_data_stats": marked_data_stats,
                    "message": "Device data added sucessfully"
                }

        except Exception, e:
            return {
                "status": False,
                "message": "Device data addition failed %s" %e
            }

db.register([Dashboard])
from flask import Response, request, abort, redirect, render_template
from app import app, db, mail
from flask_mail import Message
from bson import json_util
import json
from libs import tokenize
from libs.decorators import *
import requests
import datetime
import time

def calculateOperationTime(stopTime, startTime):

    # start =  str(startTime)
    # stop = str(stopTime)

    # a = datetime.datetime.strptime(start, '%Y-%m-%d %H:%M:%S.%f')
    # b = datetime.datetime.strptime(stop, '%Y-%m-%d %H:%M:%S.%f')

    # d = b - a
    d = stopTime - startTime
    return int(d)

@app.route('/data/add', methods=["POST"])
def addDeviceData(**kwargs):
    try:
        request_data = request.get_json(force=True)
        print request_data
        if request_data['status'] == "failure":
            print "inside stop time"
            request_data['operation_time'] = 0
            response = db.Data.addDeviceData(request_data)
        else:
            request_data['layout_name'] = request_data['marked_data'].split(" ")[0]
            request_data['marked_data'] = request_data['marked_data'].split(" ", 1)[1]
            stopTime = request_data['finish_time']
            startTime = request_data['start_time']
            operation_time = calculateOperationTime(stopTime, startTime)
            request_data['operation_time'] = operation_time
            add_operation_time_response = db.Data.addDeviceOperationTime(operation_time, request_data['serial_id'])
            if add_operation_time_response['status']:
                response = db.Data.addDeviceData(request_data)
            else:
                response = add_operation_time_response
        

        return Response(json.dumps(response, default=json_util.default), 
            mimetype="application/json")

    except Exception, e:
		return Response(json.dumps({
    			"status": False,
    			"message": "Invalid add device data request: %s" %e
    		}, default=json_util.default), mimetype="application/json")	


@app.route('/data/datalogs/<serial_id>', methods=['POST'])
@authentication_required
def fetchDeviceData(**kwargs):
    try:
        request_data = request.get_json(force=True)
        # start_date = time.time() - 3600*30
        # end_date = time.time()
        start_date = request_data['start_date']
        end_date = request_data['end_date']     
        response = db.Data.getDatalogs(kwargs['serial_id'], start_date, end_date)
        return Response(json.dumps(response, default=json_util.default), mimetype="application/json")
    except Exception, e:
		return Response(json.dumps({
    			"status": False,
    			"message": "Invalid add device data request: %s" %e
    		}, default=json_util.default), mimetype="application/json")

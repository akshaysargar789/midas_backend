from flask import Flask, render_template, Response, request
from app import app, db
import json
from bson import ObjectId
from flask_mongokit import MongoKit, Document
import datetime
import time

class Data(Document):
    __collection__ = "data"
    structure = {
        "name": str,
    }

    def addDeviceData(self, device_data):
        try:
            print "addDeviceData"
            print device_data
            device_data_id = db.data.insert(device_data)
            return {
                    "status": True,
                    "message": "Device data added sucessfully"
                }

        except Exception, e:
            return {
                "status": False,
                "message": "Device data addition failed %s" %e
            }

    def getDeviceStartTime(self, device_data):
        try:
            device_data= list(db.data.find({"$and" : [{"serial_id": device_data['serial_id'] }, {"value": "start" }] }).sort([('ts', -1)]).limit(1))
            return {
                    "status": True,
                    "data": device_data
                }

        except Exception, e:	
            return {
                "status": False,
                "message": "Device with start time not found %s" %e
            }

    def addDeviceOperationTime(self, operation_time, serial_id):
        try:
            device_data = list(db.devices.find({"serial_id": serial_id}))
            
            if 'total_operation_time' in device_data[0]:
                timestamp = device_data[0]['total_operation_time']
                total_operation_time = operation_time + timestamp
            else:
                total_operation_time = operation_time

            if 'components_marked' in device_data[0]:
                components_marked = device_data[0]['components_marked'] + 1
            else:
                components_marked = 1

            db.devices.update(
                {"serial_id": serial_id},
                {
                    "$set": {
                        'total_operation_time': total_operation_time,
                        'components_marked': components_marked
                    }
                }
            )
            return {
                "status": True,
                "message": "Device total operation time added"
            }
        except Exception, e:	
            return {
                    "status": False,
                    "message": "Device not found with correct serial_id %s" %e
                }
    
    def getDatalogs(self, serial_id, start_date, end_date):
        try:
            data = list(db.data.find({'serial_id': str(serial_id), "start_time": {"$gte": int(start_date), "$lte": int(end_date) }}))
            data.reverse()
            if len(data) == 0:
                return {
                    "status": False,
                    "message": "No data logs to fetch"
                }
            else:
                return {
                    "status": True,
                    "data": data,
                    "message": "Data logs fetched sucessfully"
                }
            

        except Exception, e:
            return {
                "status": False,
                "message": "Data logs fetching failed %s" %e
            }
						            

db.register([Data])
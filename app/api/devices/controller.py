from flask import Response, request, abort, redirect, render_template
from app import app, db, mail
from flask_mail import Message
from bson import json_util
import json
from libs import tokenize
from libs.decorators import *

import requests
import datetime
import time

@app.route('/devices/add', methods=["POST"])
@authentication_required
def addNewDevice(**kwargs):
    try:
        if kwargs['payload']['role'] == "user":
            device_data = request.get_json(force=True)
            if not(db.Device.deviceExists(device_data["name"], kwargs['payload']['email'])):
                response = db.Device.addDevice(device_data, kwargs['payload']['email'])
            else:
                response = {
                        "status": False,
                        "message": "Device is already added"
                    }
        else:
            response = {
                    "status": False,
                    "message": "Token Invalid"
                }

        return Response(
                json.dumps(response, default=json_util.default), 
                mimetype="application/json"
            )    
    except Exception, e:
            print "Error: %s" %e
            return False

@app.route('/devices/all', methods=["GET"])
@authentication_required
def returnAllDevices(**kwargs):
	if kwargs['payload']['role'] == "user":
		response = db.Device.getAllDevicesByUserEmail(kwargs['payload']['email'])
		return Response(json.dumps(response, default=json_util.default), 
			mimetype="application/json")
	else:
		return Response(json.dumps({
				"status": False,
				"message": "You do not have access to this resource"
			}, default=json_util.default), 
			mimetype="application/json")

@app.route('/devices/delete/<device_id>', methods=["DELETE"])
@authentication_required
def deleteDevice(**kwargs):
    try:
		if kwargs['payload']['role'] == "user" and db.Device.deviceExistsById(kwargs['device_id'], kwargs['payload']['email']):
			response = db.Device.deleteDevice(kwargs["device_id"])			
			return Response(json.dumps(response, default=json_util.default), 
					mimetype="application/json")	
		else:
			if not(db.Device.deviceExistsById(kwargs['device_id'], kwargs['payload']['email'])):
				return Response(json.dumps({
						"status": False,
						"message": "Device does not exist"
					}, default=json_util.default), 
					mimetype="application/json")
			else:
				return Response(json.dumps({
						"status": False,
						"message": "Authorization Failure"
					}, default=json_util.default), 
					mimetype="application/json")
    except AttributeError:
        return Response(json.dumps({
            "status": False,
            "message": "Invalid Request"
        }, default=json_util.default), mimetype="application/json")

@app.route('/devices/activate/<device_id>', methods=["POST"])
@authentication_required
def activateDevice(**kwargs):
    try:
        if kwargs['payload']['role'] == "user" and db.Device.deviceExistsById(kwargs['device_id'], kwargs['payload']['email']):
            response = db.Device.activateDevice(kwargs['device_id'])
            payload = {
                "device_id": kwargs['device_id']
            }
            response["device_token"] = tokenize.createToken(payload)			
            return Response(json.dumps(response, default=json_util.default), 
                    mimetype="application/json")
    except Exception, e:
        return Response(json.dumps({
            "status": False,
            "message": "Request failed : %s" %e
        }, default=json_util.default),
        mimetype="application/json")

@app.route('/devices/device/<serial_id>', methods=["GET"])
@authentication_required
def getDeviceBySerialID(**kwargs):
    try:
        response = db.Device.returnDeviceBySerialID(kwargs["serial_id"])
        print response
        return Response(json.dumps({
                    "status": True,
                    "message": "Device Fetched Successfully",
                    "data": response
                }, default=json_util.default),
                mimetype="application/json")
    except Exception, e:
        return Response(json.dumps({
                "status": False,
                "message": "Request failed : %s" %e
            }, default=json_util.default),
            mimetype="application/json")

@app.route('/devices/edit/name/<device_id>', methods=["PUT"])
@authentication_required
def changeDeviceName(**kwargs):
    try:
        if kwargs['payload']['role'] == "user" and db.Device.deviceExistsById(kwargs['device_id'], kwargs['payload']['email']):
            device_data = request.get_json(force=True)
            if not db.Device.deviceExists(device_data["name"], kwargs['payload']['email']):
                response = db.Device.modifyDeviceName(kwargs["device_id"], device_data["name"])
            else:
                response = {
                    "status": False,
                    "message": "Device name already exists"
                }
            return Response(json.dumps(response, default=json_util.default), 
            mimetype="application/json")
        else:
            return Response(json.dumps({
                    "status": False,
                    "message": "Authorization failure"
                }, default=json_util.default), 
                mimetype="application/json")
    except Exception, e:
        return Response(json.dumps({
            "status": False,
            "message": "Invalid edit device request: %s" %e
        }, default=json_util.default), mimetype="application/json")	

@app.route('/devices/edit/description/<device_id>', methods=["PUT"])
@authentication_required
def changeDeviceDescription(**kwargs):
    try:
        if kwargs['payload']['role'] == "user" and db.Device.deviceExistsById(kwargs['device_id'], kwargs['payload']['email']):
            device_data = request.get_json(force=True)
            response = db.Device.modifyDeviceDescription(kwargs["device_id"], device_data["description"])
            return Response(json.dumps(response, default=json_util.default), 
            mimetype="application/json")
        else:
            return Response(json.dumps({
                    "status": False,
                    "message": "Authorization failure"
                }, default=json_util.default), 
                mimetype="application/json")
    except Exception, e:
        return Response(json.dumps({
            "status": False,
            "message": "Invalid edit device request: %s" %e
        }, default=json_util.default), mimetype="application/json")


@app.route('/devices/service/<device_id>', methods=["POST"])
@authentication_required
def addServiceTime(**kwargs):
    try:
        request_data = request.get_json(force=True)
        service_time = time.time()
        response= db.Device.addServiceTime(kwargs["device_id"], service_time)
        return Response(json.dumps(response, default=json_util.default), mimetype="application/json")
    except Exception, e:
        return Response(json.dumps({
            "status": False,
            "message": "Invalid service request: %s" %e
        }, default=json_util.default), mimetype="application/json")

@app.route('/devices/components_marked/<device_id>', methods=["POST"])
@authentication_required
def resetComponent(**kwargs):
    try:
        response= db.Device.resetComponent(kwargs["device_id"])
        return Response(json.dumps(response, default=json_util.default), mimetype="application/json")
    except Exception, e:
        return Response(json.dumps({
            "status": False,
            "message": "Invalid service request: %s" %e
        }, default=json_util.default), mimetype="application/json")

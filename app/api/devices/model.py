from flask import Flask, render_template, Response, request
from app import app, db
import json
from bson import ObjectId
from flask_mongokit import MongoKit, Document
import datetime
import time

class Device(Document):
	__collection__ = "devices"
	structure = {
		"name": str,
		"type": str,
		"description": str,
		"serial_id": str,
		"is_active" : bool,
		"status": str,
		"owner_email": str,
		"attached_profile": str,
		"ssid": str,
		"mac_id": str,
		"firmware_version": str,
		"ota_timestamp": str,
		"last_boot_ts": str,
		"last_update_ts": str,
		"ip": str,
		"extra" : dict
	}
	required_fields = ["name", "type"]	
	default_values = {"is_active": False, "extra": {}}
	use_dot_Notification = True

	def addDevice(self, device_data, owner_email):
		try:
			device_data["owner_email"] = owner_email
			device_data["is_active"] =  False
			device_data["status"] =  "offline"
			device_id = db.devices.insert(device_data)

			return {
					"status": True,
					"device_id": device_id,
					"message": "Device added sucessfully"
				}

		except Exception, e:
			print "Error: %s" %e
			return {
				"status": False,
				"message": "Device addition failed %s" %e
			}

	def deviceExists(self, device_name, owner_email):
		devices = list(db.devices.find({"name": device_name, "owner_email": owner_email}))
		if not devices:
			return False
		else:
			return True

	def getAllDevicesByUserEmail(self, user_email):
		try:
			devices = list(self.find({"owner_email": user_email}))
			devices.reverse()
			return {
						"status": True,
						"devices": devices,
						"message": "Devices fetched successfully"
					}
		except Exception, e:
			return {
				"status": False,
				"message": "Devices not fetched %s" %e
			}

	def deviceExistsById(self, device_id, owner_email):
		devices = list(db.devices.find({"_id": ObjectId(device_id), "owner_email": owner_email}))
		if len(devices) == 0:
			print devices
			return False
		else:
			return True

	def deleteDevice(self, device_id):
		try:
			db.devices.remove({"_id": ObjectId(device_id)})
			return {
				"status": True,
				"message": "Device deleted successfully"
			}
		except:
			return {
				"status": False,
				"message": "Device deletion failed"
			}

	def activateDevice(self, device_id):
		try:
			db.devices.update(
					{"_id": ObjectId(device_id)},
					{
						"$set": {
							'is_active': True
						}
					}
				)
			# Check if firmware version is the latest, if not, send the url of the latest OTA firmware
			return {
				"status": True,
				"message": "Device activated successfully"
			}
		except Exception, e:
			return {
				"status": False,
				"message": "Device activation failed %s" %e
			}  

	def modifyDeviceName(self, device_id, new_name):
		if len(list(db.devices.find({"_id": ObjectId(device_id)}))) is not 0:
			db.devices.update(
				{"_id": ObjectId(device_id)},
				{
					"$set": {
						'name': new_name
					}
				}
			)
			return {
				"status": True,
				"message": "Device name changed successfully"
			}
		else:
			return {
				"status": False,
				"message": "Device does not exist"
			}

	def modifyDeviceDescription(self, device_id, new_description):
		if len(list(db.devices.find({"_id": ObjectId(device_id)}))) is not 0:
			db.devices.update(
				{"_id": ObjectId(device_id)},
				{
					"$set": {
						'description': new_description
					}
				}
			)
			return {
				"status": True,
				"message": "Device description changed successfully"
			}
		else:
			return {
				"status": False,
				"message": "Device does not exist"
			} 

	def returnDeviceBySerialID(self, serial_id):
		try:
			device = db.devices.find_one({"serial_id": serial_id})
			return device
		except Exception, e:
			return e
	
	def addServiceTime(self, device_id, service_time):
		try:
			device = db.devices.update(
				{"_id": ObjectId(device_id)},
				{
					"$set": {
						'service_time': int(service_time)
					}
				}
			)
			return {
				"status": True,
				"message": "Service time updated successfully"
			}
		except Exception, e:
			return {
				"status": False,
				"message": "Service time updation failed: %s" %e
			}
	
	def resetComponent(self, device_id):
		try:
			device = db.devices.update(
				{"_id": ObjectId(device_id)},
				{
					"$set": {
						'components_marked': 0
					}
				}
			)
			return {
				"status": True,
				"message": "Components successfully reset to 0"
			}
		except Exception, e:
			return {
				"status": False,
				"message": "Components unsuccessfully reset: %s" %e
			}

	

db.register([Device])
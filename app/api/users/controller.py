from flask import Response, request, abort, redirect, render_template
from app import app, db, mail
from flask_mail import Message
from werkzeug.security import generate_password_hash, check_password_hash
from bson import json_util
import json
from libs import tokenize
from itsdangerous import URLSafeSerializer, BadSignature 

@app.route('/users/register', methods=['POST'])
def registerUser():
		user_data = request.get_json(force=True)
		user_data["password"] = generate_password_hash(user_data["password"])
		response = db.User.addUser(user_data)
		if response["status"]:
			# Send verification mail
			user = db.User.getUserByEmail(user_data["email"])
			payload = getActivationLink(user)

			params = json.dumps({
				"payload": payload,
				"email": user_data["email"]
			})
			
			sendMail(params, 'verification')

			return Response(
				json.dumps(response, default=json_util.default), 
				mimetype="application/json"
			)

		else:
			return Response(
					json.dumps(response, default=json_util.default), 
					mimetype="application/json"
				)

@app.route('/users/login', methods=["POST"])
def loginUser():
	user_data = request.get_json(force=True)
	response = db.User.checkUserCredentials(user_data)
	if response["status"]:
		payload = {
			"role": response["role"],
			"email": user_data["email"],
			"user_id": response["user_id"]
		}
		response["user_token"] = tokenize.createToken(payload) 
		return Response(
			json.dumps(response, default=json_util.default), 
			mimetype="application/json"
		)
	else:
		return Response(
			json.dumps(response, default=json_util.default), 
			mimetype="application/json"
		)

@app.route('/forgot-password', methods=["POST"])
def forgotPassword():
	user_data = request.get_json(force=True)
	response = db.User.checkUserCredentialsForPassword(user_data)
	if response["status"]:
		print "response user"
		print response['user']
		payload = getActivationLink(response['user'])

		params = json.dumps({
			"payload": payload,
			"email": user_data["email"]
		})
		sendMail(params, 'passwordReset')
		
	return Response(
		json.dumps(response, default=json_util.default), 
		mimetype="application/json"
		)

def getSerializer(secret_key=None):
	if secret_key is None:
		secret_key = app.config["SECRET_KEY"]
	return URLSafeSerializer(secret_key)

def getActivationLink(user):
	s = getSerializer()
	payload = s.dumps(str(user["_id"]))
	return payload

def sendMail(params, mailType):
	params = json.loads(params)
	payload = params["payload"]
	user_email = params["email"] 

	if(mailType == 'verification'):
		msg = Message("Midas Account Verification",
		sender=("Midas Notifications", app.config.get("MAIL_USERNAME")),
		recipients=[user_email])
		print app.config['BASE_URL']
		msg.html = render_template('email_confirmation.html', activation_link = app.config['BASE_URL'] + '/users/activate/' + payload)
	else:
		msg = Message("Midas Account Password Reset",
		sender=("Midas Notifications", app.config.get("MAIL_USERNAME")),
		recipients=[user_email])
		print app.config['BASE_URL']
		msg.html = render_template('email_confirmation.html', activation_link = app.config['BASE_URL'] + '/users/reset-password/' + payload)
	
	mail.send(msg)
	return "Mail Sent"

@app.route('/users/reset-password/<reset_link>' , methods=["GET","POST"])
def resetPassword(reset_link):
	s = getSerializer()
	user_id = s.loads(reset_link)
	user_data = db.User.getUserById(user_id)

	if(request.method == "GET"):
		return render_template('reset_password_form.html', email = user_data["email"], reset_link = reset_link)
	elif request.method == "POST":
		user_json= request.get_json(force=True)
		query = {
			"email" : user_data["email"],
			"password" : generate_password_hash(user_json["password"])
		}

		user = db.User.resetUserPassword(query)
		if user["nModified"]:
			response = {
			"status" : True, 
			"message" :  "Password Reset Successful"
			}
			return Response(json.dumps(response, default=json_util.default), mimetype="application/json")
		else:
			response = {
			"status" : False, 
			"message" :  "Password Reset Failed "
			}
			return Response(json.dumps(response, default=json_util.default), mimetype="application/json")

@app.route('/users/activate/<payload>')
def activateUser(payload):
	print "Inside activateUser"
	print payload

	s = getSerializer()
	try:
		user_id = s.loads(payload)
		print user_id
	except BadSignature:
		abort(404)

	user = db.User.getUserById(user_id)

	if user is not None:
		response = db.User.verifyUser(user["_id"])
		if response["status"]:
			return redirect('https://www.google.com')
		else:
			return redirect('https://www.wikipedia.com')
			

@app.route('/', methods=["GET"])
def landingPage():
	return "Hello"


from app import db
from bson import json_util, ObjectId
from flask_mongokit import MongoKit, Document
from werkzeug.security import generate_password_hash, check_password_hash
import uuid

class User(Document):
	__collection__ = "users"
	structure = {
		"user_id": str,
		"name": str,
		"email": str,
		"contact": str,
		"organization": str,
		"is_verified": bool,
		"role": str,
		"extra": dict
	}
	required_fields = ["name", "email"]
	default_values = {"is_verified": False, "role": "user", "extra": {}}
	use_dot_notification = True

	def getUserByEmail(self, email):
		user = self.find_one({"email": email})
		return user
	
	def getUserById(self, id):
		user = self.find_one({"_id": ObjectId(id)})
		return user
	
	def verifyUser(self, id):
		# Update User["is_verified"] to True
		user = db.users.update(
			{"_id": id},
			{
				"$set":{
					'is_verified': True
				}
			}
		)
		return {
			"status": True,
			"message": "User Verified Successfully"
		}

	def addUser(self, user_data):
		try:
			user = self.getUserByEmail(user_data["email"])
			if user is None:
				user_data["is_verified"] = False
				user_data["role"] = "user"
				user_data["extra"] = {}
				user_data["user_id"] = str(uuid.uuid4())[:8].upper()

				db.users.insert(user_data)

				return {
					"status": True,
					"message": "User Registration successful, please check mail for further information."
				}
			else:
				return {
					"status": False,
					"message": "This Email ID is already registered."
				}

		except Exception, e:
			return {
				"status": False,
				"message": "User Registration failed."
			}

	def checkUserCredentials(self, user_data):
		try:
			user = self.getUserByEmail(user_data["email"])
			if user is not None:
				if check_password_hash(user["password"], user_data["password"]):
					if user["is_verified"]:
						return {
							"status": True,
							"role": user["role"],
							"name": user["name"].split(' ')[0],
							"user_id": user["user_id"],
							"message": "Login Successful"
						}
					else:
						return {
							"status": False,
							"message": "Email not Verified. Verify your Email"
						}
				else:
					return {
						"status": False,
						"message": "Incorrect Credentials"
					}
			else:
				return {
					"status": False,
					"message": "This Email ID is not registered. Please Register before logging in."
				}
		except TypeError:
			return {
				"status": False,
				"message": "This Email ID is not registered. Please Register before logging in."
			} 
		except Exception, e:
			print "Exception"
			print e
			return {
				"status": False,
				"message": "User Login failed."
			}

	def checkUserCredentialsForPassword(self, user_data):
		try:
			user = self.getUserByEmail(user_data["email"])
			if user is not None:
				return {
					"status": True,
					"user": user,
					"message": "Password Reset link has been sent to the registered Email ID."
				}
			else:
				return {
					"status": False,
					"message": "This Email ID is not registered. Please Register before logging in."
				}
		except TypeError:
			return {
				"status": False,
				"message": "This Email ID is not registered. Please Register before logging in."
			} 
		except Exception, e:
			return {
				"status": False,
				"message": "Password reset failed."
			}
	
	def resetUserPassword(self, user_data):
		try:
			user_id = self.getUserByEmail(user_data["email"])
			print "user_id"
			print user_id
			user = db.users.update(
				{"_id": user_id["_id"]},
				{
					"$set":{
						'password': user_data["password"]
					}
				}, upsert=False, multi=False)
			if user is not None:
				return user
			else:
				return False
		except Exception, e:
			print e
			return None

db.register([User])
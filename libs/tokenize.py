import jwt
from app import app

def createToken(payload):
  payload = payload
  token = jwt.encode(payload, app.config.get('SECRET_KEY'), algorithm='HS256')
  return token.decode('unicode_escape')

def getPayload(token):
  try:
    payload = jwt.decode(token,app.config.get('SECRET_KEY'), algorithm='HS256')
  except jwt.ExpiredSignature:
    payload = None
  except jwt.DecodeError:
    payload = None

  return payload